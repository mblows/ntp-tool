//go:build linux
// +build linux

package main

import (
	"fmt"
	"os"
	"time"

	"golang.org/x/sys/unix"
)

// setSystemTime sets the system time on Linux using the settimeofday system call.
func setSystemTime(t time.Time) error {
	// Convert time.Time to Unix timestamp
	unixTime := t.Unix()

	// Extract the microseconds from the time.Time value
	microseconds := int64(t.Nanosecond() / 1000)

	// Set the system time using the Linux settimeofday system call
	var tv unix.Timeval
	tv.Sec = unixTime
	tv.Usec = microseconds

	err := unix.Settimeofday(&tv)
	if err != nil {
		return fmt.Errorf("failed to set system time: %w", err)
	}

	return nil
}

// isElevated returns true if the current user is root
func isElevated() bool {
	return os.Geteuid() == 0
}
