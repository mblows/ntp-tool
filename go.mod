module gitlab.com/mblows/ntp-tool

go 1.20

require github.com/xo/terminfo v0.0.0-20220910002029-abceb7e1c41e // indirect

require (
	github.com/gookit/color v1.5.4
	golang.org/x/sys v0.25.0
)
