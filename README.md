# NTP Tool

This is a command-line utility written in Go that allows you to query NTP (Network Time Protocol) servers and retrieve time information. It provides a way to check the accuracy and synchronisation of your system clock with reliable time sources.

## Features

-   Query multiple NTP servers concurrently
-   Display the following information for each NTP server:
    -   Stratum
    -   Reference ID
    -   Clock offset
    -   Round-trip delay
    -   NTP time
    -   System time
-   Calculate the weighted average offset across all queried servers based on their stratum values
-   Estimate the accurate time based on the weighted average offset
-   Set the system time based on the estimated average time
-   Support for reading NTP servers from a file or command-line arguments

## Installation

To compile and run the program, make sure you have Go installed on your system. Then, follow these steps:

1. Clone the repository:

    ```
    git clone https://gitlab.com/mblows/ntp-tool.git
    ```

2. Change to the project directory:

    ```
    cd ntp-tool
    ```

3. Build the executable:

    ```
    go build
    ```

## Command-Line Arguments

To query NTP servers using command-line arguments, run the following command:

```
./ntp-tool server1 server2 server3 ...
```

Replace `server1`, `server2`, `server3`, etc., with the hostnames or IP addresses of the NTP servers you want to query.

### File Input

To query NTP servers listed in a file, create a text file with one NTP server per line. Then, run the following command:

```
./ntp-tool -f path/to/file
```

Replace `path/to/file` with the path to the file containing the NTP servers.

### Setting the System Time

To set the system time based on the estimated accurate time, use the `-s` flag:

```
./ntp-tool -s server1 server2 server3 ...
```

**Note:** Setting the system time requires root/administrator privileges.

## Weighted Average Offset Calculation

When multiple NTP servers are queried, the tool calculates the weighted average offset across all the servers. The offset represents the difference between the NTP time and the system time. By using a weighted average, the tool assigns higher importance to the offsets from servers with lower stratum values, as they are considered more reliable and accurate (although this may not always be the case).

The weighted average offset is calculated using the following formula:

```
weightedAverageOffset = (offset1 * weight1 + offset2 * weight2 + ... + offsetN * weightN) / (weight1 + weight2 + ... + weightN)
```

where `offsetX` is the offset for each queried server, `weightX` is the weight assigned to each server based on its stratum value, and `N` is the total number of servers.

The weight for each server is calculated using the formula:

```
weight = 2^(16 - stratum)
```

This formula assigns exponentially higher weights to servers with lower stratum values, giving them more influence in the weighted average calculation.

## Example

```
./ntp-tool time.paleblue.cloud time.nist.gov time.cloudflare.com
```

Output:

```
time.paleblue.cloud
Stratum:    1
RefID:      PPS
Offset:     40ns
Delay:      1.356362ms
NTP Time:   May 04 2024 12:58:32.860121 AEST
Sys Time:   May 04 2024 12:58:32.860121 AEST

time.cloudflare.com
Stratum:    3
RefID:      10.47.8.121
Offset:     1.696809ms
Delay:      9.583246ms
NTP Time:   May 04 2024 12:58:32.865813 AEST
Sys Time:   May 04 2024 12:58:32.864117 AEST

time.nist.gov
Stratum:    1
RefID:      NIST
Offset:     -37.491323ms
Delay:      261.702876ms
NTP Time:   May 04 2024 12:58:33.078828 AEST
Sys Time:   May 04 2024 12:58:33.116319 AEST

Average Offset: -16.474258ms
Estimated Time: May 04 2024 12:58:33.099923 AEST
```
