package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"math"
	"net"
	"os"
	"strings"
	"time"

	"github.com/gookit/color"
)

const (
	// The difference between Unix epoch and NTP epoch (1970 - 1900) in seconds
	ntpOffset = 2_208_988_800

	// Format for printing time (microsecond precision)
	timeFormat = "Jan 02 2006 15:04:05.000000 MST"
)

// NTPTime struct represents the timestamp in NTP protocol
type NTPTime struct {
	Seconds  uint32 // seconds since NTP epoch
	Fraction uint32 // fractional part of the second
}

// NTPPacket struct represents the NTP packet format
type NTPPacket struct {
	Flags          uint8   // Packet control flags
	Stratum        uint8   // Server's distance from the reference clock
	Poll           int8    // Maximum interval between messages
	Precision      int8    // Precision of the server clock
	RootDelay      int32   // Round-trip delay to the reference clock
	RootDispersion uint32  // Dispersion of the server clock
	ReferenceID    uint32  // Identifier for the reference clock
	ReferenceTime  NTPTime // Last time the reference clock was updated
	OriginTime     NTPTime // Time when the request departed the client for the server
	ReceiveTime    NTPTime // Time when the request arrived at the server
	TransmitTime   NTPTime // Time when the response departed the server
}

// Results struct represents the results of the NTP query
type Results struct {
	Name    string
	Offset  time.Duration
	Delay   time.Duration
	Stratum uint8
	RefID   string
	NTPTime time.Time
	SysTime time.Time
	Error   error
}

// Unix converts NTPTime to time.Time
func (t *NTPTime) Unix() time.Time {
	// Convert NTPTime (seconds since NTP epoch) to time.Time
	sec := int64(t.Seconds - ntpOffset)
	nsec := int64(uint64(t.Fraction) * uint64(time.Second) / (1 << 32))
	return time.Unix(sec, nsec)
}

// getRefID returns the reference ID as a string
func getRefID(refID uint32, stratum uint8) string {
	// If it's a stratum-1 server, the reference ID is a 4-character ASCII string
	if stratum == 1 {
		buf := make([]byte, 4)
		binary.BigEndian.PutUint32(buf, refID)
		return string(bytes.Trim(buf, "\x00"))
	}

	// Otherwise, the reference ID is an IPv4 address
	return fmt.Sprintf("%d.%d.%d.%d",
		refID>>24,
		(refID>>16)&0xFF,
		(refID>>8)&0xFF,
		refID&0xFF,
	)
}

// queryServer queries the NTP server and outputs the results to the outCh channel
func queryServer(target string, resCh chan Results) {
	// Resolve the UDP address of the NTP server
	udpAddr, err := net.ResolveUDPAddr("udp", net.JoinHostPort(target, "123"))
	if err != nil {
		resCh <- Results{Name: target, Error: err}
		return
	}

	// Dial the NTP server
	conn, err := net.Dial("udp", udpAddr.String())
	if err != nil {
		resCh <- Results{Name: target, Error: err}
		return
	}
	defer conn.Close()

	// Set a timeout for the connection to avoid blocking indefinitely
	conn.SetDeadline(time.Now().Add(1 * time.Second))

	// Get the current system time
	now := time.Now()

	// Create an NTP packet for the request (and later for the response)
	pkt := NTPPacket{
		Flags: 0x23, // Version 4, Client
		TransmitTime: NTPTime{
			// Convert current system time to NTP time format
			Seconds:  uint32(now.Unix() + ntpOffset),
			Fraction: uint32(uint64(now.Nanosecond()) * (1 << 32) / uint64(time.Second)),
		},
	}

	// Send the NTP request packet to the server
	err = binary.Write(conn, binary.BigEndian, &pkt)
	if err != nil {
		resCh <- Results{Name: target, Error: err}
		return
	}

	// Read the NTP response packet from the server
	err = binary.Read(conn, binary.BigEndian, &pkt)
	if err != nil {
		resCh <- Results{Name: target, Error: err}
		return
	}

	// Convert NTP times to Unix times
	t4 := time.Now()              // Current system time when the response packet is received (t4)
	t1 := pkt.OriginTime.Unix()   // Time when the request departed the client for the server (t1)
	t2 := pkt.ReceiveTime.Unix()  // Time when the request arrived at the server (t2)
	t3 := pkt.TransmitTime.Unix() // Time when the response departed the server (t3)

	// Calculate the clock offset using the formula (t2 - t1 + t3 - t4) / 2
	offset := (t2.Sub(t1) + t3.Sub(t4)) / 2

	// Calculate the round-trip delay using the formula (t4 - t1) - (t3 - t2)
	delay := (t4.Sub(t1) - t3.Sub(t2))
	currentTime := t4.Add(offset)

	// Populate the results struct
	results := Results{
		Name:    target,
		Offset:  offset,
		Delay:   delay,
		Stratum: pkt.Stratum,
		RefID:   getRefID(pkt.ReferenceID, pkt.Stratum),
		NTPTime: currentTime,
		SysTime: t4,
		Error:   err,
	}

	// Send the results to the outCh channel
	resCh <- results
}

// loadServers reads NTP servers from a file, one per line
func loadServers(file string) ([]string, error) {
	// Open the file for reading
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Read the NTP servers from the file
	var servers []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		// Skip empty lines and comments
		line := strings.TrimSpace(strings.Split(scanner.Text(), "#")[0])
		if line == "" {
			continue
		}

		servers = append(servers, line)
	}

	return servers, scanner.Err()
}

// weightedAverage calculates the weighted average of the offsets based on the stratum
func weightedAverage(results []Results) time.Duration {
	var totalWeightedOffset float64
	var totalWeight float64

	for _, result := range results {
		weight := math.Pow(2, float64(16-result.Stratum))
		totalWeightedOffset += float64(result.Offset) * weight
		totalWeight += weight
	}

	if totalWeight == 0 {
		return 0
	}

	return time.Duration(totalWeightedOffset / totalWeight)
}

func main() {
	file := flag.String("f", "", "Path to the file containing NTP servers (one per line)")
	setTime := flag.Bool("s", false, "Set the system time based on the average offset (requires elevated privileges)")
	flag.Parse()

	var (
		servers []string
		err     error
	)

	// Only allow setting the time if the user is root/administrator
	if !isElevated() && *setTime {
		color.Println("<fg=red;op=bold>Setting the time requires elevated privileges.</>")
		os.Exit(1)
	}

	// Read NTP servers from the file if the -f option is provided
	if *file != "" {
		servers, err = loadServers(*file)
		if err != nil {
			fmt.Println("Error reading NTP servers from file:", err)
			os.Exit(1)
		}
	} else {
		// Read NTP servers from the command-line arguments
		servers = flag.Args()
	}

	if len(servers) == 0 {
		fmt.Println("Please provide at least one NTP server as a command-line argument or using the -f option.")
		os.Exit(1)
	}

	var (
		resCh        = make(chan Results, len(servers)) // Channel to receive the results of the NTP queries
		errors       []Results                          // Errors that occurred during the NTP queries
		validResults []Results                          // Valid results of the NTP queries
		totalOffset  time.Duration                      // Total offset of all valid servers
	)

	// Query all the servers in parallel
	for _, server := range servers {
		go queryServer(server, resCh)
	}

	// Print the results of the NTP queries
	for range servers {
		result := <-resCh
		if result.Error != nil {
			errors = append(errors, result)
			continue
		}

		// Print the results
		color.Printf("<fg=lightBlue;op=bold>%s</>\n", result.Name)
		color.Printf("<lightGreen>Stratum</>:    %d\n", result.Stratum)
		color.Printf("<lightGreen>RefID</>:      %s\n", result.RefID)
		color.Printf("<lightGreen>Offset</>:     %s\n", result.Offset)
		color.Printf("<lightGreen>Delay</>:      %s\n", result.Delay)
		color.Printf("<lightGreen>NTP Time</>:   %s\n", result.NTPTime.Format(timeFormat))
		color.Printf("<lightGreen>Sys Time</>:   %s\n\n", result.SysTime.Format(timeFormat))

		// Calculate the total offset
		totalOffset += result.Offset
		validResults = append(validResults, result)
	}

	// Calculate the average offset weighted by stratum
	averageOffset := weightedAverage(validResults)

	// Print the average offset if there are multiple valid servers
	if len(validResults) > 1 {
		color.Printf("<fg=lightYellow;op=bold>Average Offset</>: %s\n", averageOffset)
		color.Printf("<fg=lightYellow;op=bold>Estimated Time</>: %s\n", time.Now().Add(averageOffset).Format(timeFormat))
	}

	// Set the system time based on the average offset if the -s flag is set
	if *setTime {
		err := setSystemTime(time.Now().Add(averageOffset))
		if err != nil {
			color.Printf("\n<fg=red;op=bold>%s</>\n", err)
		} else {
			color.Println("\n<fg=lightGreen;op=bold>System time set successfully.</>")
		}
	}

	// Print any errors that occurred during the NTP queries, if any
	if len(errors) > 0 {
		fmt.Println()
		for _, errs := range errors {
			color.Printf("<fg=red;op=bold>%s</>: %s", errs.Name, errs.Error)
			color.Println()
		}
	}
}
