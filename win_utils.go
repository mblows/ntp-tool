//go:build windows
// +build windows

package main

import (
	"fmt"
	"time"
	"unsafe"

	"golang.org/x/sys/windows"
)

var (
	modkernel32       = windows.NewLazySystemDLL("kernel32.dll")
	procSetSystemTime = modkernel32.NewProc("SetSystemTime")
)

// SYSTEMTIME structure as required by Windows API
type SYSTEMTIME struct {
	Year         uint16
	Month        uint16
	DayOfWeek    uint16
	Day          uint16
	Hour         uint16
	Minute       uint16
	Second       uint16
	Milliseconds uint16
}

// setSystemTime sets the system time on Windows using the SetSystemTime API
func setSystemTime(t time.Time) error {
	tUTC := t.UTC()
	// Populate the SYSTEMTIME structure
	st := SYSTEMTIME{
		Year:         uint16(tUTC.Year()),
		Month:        uint16(tUTC.Month()),
		DayOfWeek:    uint16(tUTC.Weekday()),
		Day:          uint16(tUTC.Day()),
		Hour:         uint16(tUTC.Hour()),
		Minute:       uint16(tUTC.Minute()),
		Second:       uint16(tUTC.Second()),
		Milliseconds: uint16(tUTC.Nanosecond() / 1e6),
	}

	// Call SetSystemTime
	r1, _, err := procSetSystemTime.Call(uintptr(unsafe.Pointer(&st)))
	if r1 == 0 {
		return fmt.Errorf("failed to set system time: %w", err)
	}
	return nil
}

// isElevated returns true if the current user is an administrator
func isElevated() bool {
	var sid *windows.SID
	// See https://docs.microsoft.com/en-us/windows/desktop/api/securitybaseapi/nf-securitybaseapi-checktokenmembership
	// https://coolaj86.com/articles/golang-and-windows-and-admins-oh-my/

	err := windows.AllocateAndInitializeSid(
		&windows.SECURITY_NT_AUTHORITY,
		2,
		windows.SECURITY_BUILTIN_DOMAIN_RID,
		windows.DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&sid)
	if err != nil {
		return false
	}
	defer windows.FreeSid(sid)

	token := windows.Token(0)
	member, err := token.IsMember(sid)
	if err != nil {
		return false
	}

	return member
}
